package mutex

import "sync"

type MUTEX struct {
	MutexMap sync.RWMutex
	MapData  map[string]*sync.RWMutex
}
