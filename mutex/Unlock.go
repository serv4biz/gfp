package mutex

func (me *MUTEX) Unlock(key string) {
	me.MutexMap.Lock()
	mtx, ok := me.MapData[key]
	me.MutexMap.Unlock()
	if ok {
		mtx.Unlock()
	}
}
