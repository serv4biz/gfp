package mutex

func (me *MUTEX) RUnlock(key string) {
	me.MutexMap.Lock()
	mtx, ok := me.MapData[key]
	me.MutexMap.Unlock()
	if ok {
		mtx.RUnlock()
	}
}
