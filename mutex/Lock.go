package mutex

import "sync"

func (me *MUTEX) Lock(key string) {
	me.MutexMap.Lock()
	mtx, ok := me.MapData[key]
	if !ok {
		mtx = new(sync.RWMutex)
		me.MapData[key] = mtx
	}
	me.MutexMap.Unlock()
	mtx.Lock()
}
