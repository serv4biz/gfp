package mutex

import "sync"

func New() *MUTEX {
	mtx := new(MUTEX)
	mtx.MapData = make(map[string]*sync.RWMutex)
	return mtx
}
