module gitlab.com/serv4biz/gfp

go 1.24

replace gitlab.com/serv4biz/gfp => ./

require (
	github.com/go-sql-driver/mysql v1.8.1
	github.com/google/uuid v1.6.0
	github.com/lib/pq v1.10.9
)

require github.com/oklog/ulid/v2 v2.1.0

require filippo.io/edwards25519 v1.1.0 // indirect
