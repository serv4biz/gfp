package files

import (
	"io/fs"
	"os"
	"strings"
)

// WriteFile is write buffer byte to path file
func WriteFile(pathFile string, buffer []byte, perm fs.FileMode) (int, error) {
	size := len(buffer)
	err := os.WriteFile(strings.TrimSpace(pathFile), buffer, perm)
	if err != nil {
		return -1, err
	}
	return size, err
}
