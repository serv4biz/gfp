package files

import (
	"os"
	"strings"
)

// IsFile is check path file it is a file
func IsFile(pathFile string) bool {
	pf, err := os.Stat(strings.TrimSpace(pathFile))
	if err != nil {
		return !os.IsNotExist(err)
	}
	return pf.Mode().IsRegular()
}
