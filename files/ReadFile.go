package files

import (
	"os"
	"strings"
)

// ReadFile is read file to buffer bytes from path file
func ReadFile(pathFile string) ([]byte, error) {
	buffer, err := os.ReadFile(strings.TrimSpace(pathFile))
	if err != nil {
		return nil, err
	}
	return buffer, err
}
