package files

import (
	"os"
	"path/filepath"
)

func GetAppDir() (string, error) {
	ex, err := os.Executable()
	if err != nil {
		return "", err
	}
	exPath, err := filepath.Abs(filepath.Dir(ex))
	return exPath, err
}
