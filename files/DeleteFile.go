package files

import (
	"os"
	"strings"
)

// DeleteFile is delete file from pathfile
func DeleteFile(pathFile string) error {
	err := os.RemoveAll(strings.TrimSpace(pathFile))
	return err
}
