package files

import (
	"os"
	"strings"
)

// ModifyStamp is get last modify unix stamp from path file
func ModifyStamp(pathFile string) (int64, error) {
	pf, err := os.Stat(strings.TrimSpace(pathFile))
	if err != nil {
		return -1, err
	}
	return pf.ModTime().Unix(), err
}
