package files

import (
	"io/fs"
	"os"
	"strings"
)

// MakeDir is create a new dirctory from path
func MakeDir(pathDir string, perm fs.FileMode) error {
	err := os.MkdirAll(strings.TrimSpace(pathDir), perm)
	return err
}
