package files

import (
	"os"
	"strings"
)

// ExistFile is check exist file from pathfile
func ExistFile(pathFile string) bool {
	_, err := os.Stat(strings.TrimSpace(pathFile))
	if err != nil {
		return !os.IsNotExist(err)
	}
	return true
}
