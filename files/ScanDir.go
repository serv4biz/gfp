package files

import (
	"os"
	"strings"
)

// ScanDir is scan all file in path directory
func ScanDir(pathDir string) ([]string, error) {
	files, err := os.ReadDir(strings.TrimSpace(pathDir))
	if err == nil {
		length := len(files)
		filenames := make([]string, length)
		for i, name := range files {
			filenames[i] = name.Name()
		}
		return filenames, err
	}

	return make([]string, 0), err
}
