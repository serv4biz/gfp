package files

import (
	"os"
	"strings"
)

// IsDir is check path file it is a directory
func IsDir(pathFile string) bool {
	pf, err := os.Stat(strings.TrimSpace(pathFile))
	if err != nil {
		return !os.IsNotExist(err)
	}
	return pf.Mode().IsDir()
}
