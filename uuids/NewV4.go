package uuids

import (
	google_uuid "github.com/google/uuid"
)

func NewV4() (google_uuid.UUID, error) {
	return NewRandom()
}
