package uuids

import (
	google_uuid "github.com/google/uuid"
)

func NewRandom() (google_uuid.UUID, error) {
	return google_uuid.NewRandom()
}
