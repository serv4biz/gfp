package zlib

import (
	"compress/zlib"
)

// Encode is encode compress data from buffer bytes
func Encode(buffer []byte) ([]byte, error) {
	return EncodeLevel(buffer, zlib.DefaultCompression)
}
