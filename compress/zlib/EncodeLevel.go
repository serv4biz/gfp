package zlib

import (
	"bytes"
	"compress/zlib"
)

// EncodeLevel is encode compress data from buffer bytes by level 1 - 9
func EncodeLevel(buffer []byte, level int) ([]byte, error) {
	var b bytes.Buffer
	defer b.Reset()

	w, err := zlib.NewWriterLevel(&b, level)
	if err != nil {
		return nil, err
	}
	defer w.Close()

	_, err = w.Write(buffer)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}
