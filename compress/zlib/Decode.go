package zlib

import (
	"bytes"
	"compress/zlib"
	"io"
)

// Decode is decode compress data from buffer bytes
func Decode(buffer []byte) ([]byte, error) {
	var b bytes.Buffer
	b.Write(buffer)
	defer b.Reset()

	r, err := zlib.NewReader(&b)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	p, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}
	return p, nil
}
