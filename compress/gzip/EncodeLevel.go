package gzip

import (
	"compress/gzip"
	"io"
	"os"
)

// Encode is encode compress data from buffer bytes
func EncodeLevel(srcPath string, dstPath string, level int) error {
	originalFile, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer originalFile.Close()

	gzippedFile, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer gzippedFile.Close()

	gzipWriter, err := gzip.NewWriterLevel(gzippedFile, level)
	if err != nil {
		return err
	}
	defer gzipWriter.Close()

	_, err = io.Copy(gzipWriter, originalFile)
	if err != nil {
		return err
	}

	return gzipWriter.Flush()
}
