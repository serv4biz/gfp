package gzip

import (
	"compress/gzip"
)

// Encode is encode compress data from buffer bytes
func Encode(srcPath string, dstPath string) error {
	return EncodeLevel(srcPath, dstPath, gzip.DefaultCompression)
}
