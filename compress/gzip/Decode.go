package gzip

import (
	"compress/gzip"
	"io"
	"os"
)

// Decode is decode compress data from buffer bytes
func Decode(srcPath string, dstPath string) error {
	gzippedFile, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer gzippedFile.Close()

	gzipReader, err := gzip.NewReader(gzippedFile)
	if err != nil {
		return err
	}
	defer gzipReader.Close()

	uncompressedFile, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer uncompressedFile.Close()

	_, err = io.Copy(uncompressedFile, gzipReader)
	if err != nil {
		return err
	}
	return nil
}
