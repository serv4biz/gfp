package bigutil

import (
	"fmt"
	"math/big"
	"regexp"
	"strings"

	"gitlab.com/serv4biz/gfp/errs"
)

func FromNumberString(value string, decimal int) (*big.Int, error) {
	r := regexp.MustCompile(`[0-9\.]+`)
	if !r.MatchString(value) {
		return nil, errs.ErrValueInvalid
	}

	var tmp string = value
	if !strings.Contains(tmp, ".") {
		tmp += "."
	}

	tmps := strings.SplitN(tmp, ".", 2)
	first := tmps[0]

	r = regexp.MustCompile("(^0+)")
	first = r.ReplaceAllString(first, "")

	second := tmps[1]
	diff := decimal - len(second)
	for i := 0; i < diff; i++ {
		second += "0"
	}
	if diff < 0 {
		trim := ""
		for i := 0; i < decimal; i++ {
			trim += string(second[i])
		}
		second = trim
	}

	result, ok := big.NewInt(0).SetString(fmt.Sprint(strings.TrimSpace(first), strings.TrimSpace(second)), 10)
	if !ok {
		return nil, errs.ErrCantConvertValue
	}
	return result, nil
}
