package bigutil

import (
	"math/big"
	"strings"
)

func ToNumberString(val *big.Int, decimal int) (string, error) {
	bint, err := ToString(val)
	if err != nil {
		return "", err
	}
	buffs := []rune(bint)

	result := ""
	length := len(buffs)
	count := 0
	for i := (length - 1); i >= 0; i-- {
		ch := buffs[i]
		result = string(ch) + result

		count++
		if count == decimal {
			result = "." + result
		}
	}
	result = strings.Trim(result, ".")

	dif := decimal - length
	if dif >= 0 {
		for i := 0; i < dif; i++ {
			result = "0" + result
		}
		result = "0." + result
	}
	return result, nil
}
