package bigutil

import (
	"fmt"
	"math/big"
)

func FromNumber(val float64, decimal int) (*big.Int, error) {
	data := fmt.Sprintf(fmt.Sprint("%.", decimal, "f"), val)
	return FromNumberString(data, decimal)
}
