package bigutil

import (
	"math/big"
	"strconv"
)

func ToNumber(val *big.Int, decimal int) (float64, error) {
	result, err := ToNumberString(val, decimal)
	if err != nil {
		return 0.0, err
	}
	return strconv.ParseFloat(result, 64)
}
