package bigutil

import (
	"errors"
	"math/big"
)

func ToString(val any) (string, error) {
	switch val := val.(type) {
	case *big.Int:
		return val.String(), nil
	case big.Int:
		return val.String(), nil
	case string:
		result, ok := big.NewInt(0).SetString(val, 10)
		if ok {
			return result.String(), nil
		}
	case float64:
		return big.NewInt(int64(val)).String(), nil
	case float32:
		return big.NewInt(int64(val)).String(), nil
	case int:
		return big.NewInt(int64(val)).String(), nil
	case int8:
		return big.NewInt(int64(val)).String(), nil
	case int16:
		return big.NewInt(int64(val)).String(), nil
	case int32:
		return big.NewInt(int64(val)).String(), nil
	case int64:
		return big.NewInt(int64(val)).String(), nil
	case uint:
		return big.NewInt(int64(val)).String(), nil
	case uint8:
		return big.NewInt(int64(val)).String(), nil
	case uint16:
		return big.NewInt(int64(val)).String(), nil
	case uint32:
		return big.NewInt(int64(val)).String(), nil
	case uint64:
		return big.NewInt(int64(val)).String(), nil
	case uintptr:
		return big.NewInt(int64(val)).String(), nil
	}
	return "", errors.New("can't convert BigInt to string")
}
