package sqlutil

import (
	"fmt"
)

// Matching SQL Condition
func Match(key string, op string, val any) string {
	switch val := val.(type) {
	case string:
		return fmt.Sprint(key, " ", op, " ", Quote(val))
	case bool:
		if val {
			return fmt.Sprint(key, " ", op, " true")
		}
		return fmt.Sprint(key, " ", op, " false")
	}
	return fmt.Sprint(key, " ", op, " ", val)
}
