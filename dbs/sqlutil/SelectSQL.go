package sqlutil

import (
	"fmt"
	"strings"
)

// SelectSQL is automatic build select sql
func SelectSQL(txtTable string, txtColumn string, txtWhere string, txtOrder string, intOffset int, intLimit int) string {
	if strings.TrimSpace(txtTable) == "" {
		txtTable = "dummy"
	}

	if strings.TrimSpace(txtColumn) == "" {
		txtColumn = "*"
	}

	if strings.TrimSpace(txtWhere) != "" {
		txtWhere = fmt.Sprint(" WHERE ", txtWhere)
	}

	if strings.TrimSpace(txtOrder) != "" {
		txtOrder = fmt.Sprint(" ORDER BY ", txtOrder)
	}

	txtOffset := ""
	if intOffset >= 0 {
		txtOffset = fmt.Sprint(" OFFSET ", intOffset)
	}

	txtLimit := ""
	if intLimit >= 0 {
		txtLimit = fmt.Sprint(" LIMIT ", intLimit)
	}

	return strings.TrimSpace(fmt.Sprint("SELECT ", txtColumn, " FROM ", txtTable, txtWhere, txtOrder, txtOffset, txtLimit))
}
