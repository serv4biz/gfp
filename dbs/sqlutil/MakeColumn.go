package sqlutil

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

func MakeColumn(jsaColumn *jsons.Array) string {
	txtColumn := ""
	if jsaColumn.Length() > 0 {
		jsaColumn.EachString(func(index int, value string) error {
			txtColumn += " , " + value
			return nil
		})
	}
	return strings.TrimPrefix(txtColumn, " , ")
}
