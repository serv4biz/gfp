package sqlutil

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// UpdateSQL is automatic build update sql
func UpdateSQL(txtTable string, jsoData *jsons.Object, txtWhere string) string {
	txtSet := ""
	arrColumns := jsoData.GetKeys()
	for _, columnName := range arrColumns {

		switch jsons.GetType(jsoData.Any(columnName)) {
		case jsons.TYPE_OBJECT:
			obj, _ := jsoData.GetObject(columnName)
			txtJSON, _ := obj.ToString()
			txtSet = fmt.Sprint(txtSet, columnName, " = ", Quote(txtJSON), ",")
		case jsons.TYPE_ARRAY:
			obj, _ := jsoData.GetArray(columnName)
			txtJSON, _ := obj.ToString()
			txtSet = fmt.Sprint(txtSet, columnName, " = ", Quote(txtJSON), ",")
		case jsons.TYPE_STRING:
			txtText, _ := jsoData.GetString(columnName)
			txtSet = fmt.Sprint(txtSet, columnName, " = ", Quote(txtText), ",")
		case jsons.TYPE_NUMBER:
			dblNumber, _ := jsoData.GetNumber(columnName)
			txtSet = fmt.Sprint(txtSet, columnName, " = ", dblNumber, ",")
		case jsons.TYPE_BOOL:
			isPass, _ := jsoData.GetBool(columnName)
			txtSet = fmt.Sprint(txtSet, columnName, " = ", isPass, ",")
		case jsons.TYPE_NULL:
			txtSet = fmt.Sprint(txtSet, columnName, " = NULL,")
		}
	}
	txtSet = strings.TrimSpace(txtSet)
	txtSet = strings.Trim(txtSet, ",")

	if strings.TrimSpace(txtWhere) != "" {
		txtWhere = fmt.Sprint(" WHERE ", txtWhere)
	}

	return strings.TrimSpace(fmt.Sprint("UPDATE ", txtTable, " SET ", txtSet, txtWhere))
}
