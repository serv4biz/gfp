package sqlutil

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

func MakeOrder(jsaOrder *jsons.Array) string {
	txtSort := ""
	if jsaOrder.Length() > 0 {
		jsaOrder.EachString(func(index int, value string) error {
			txtSort += " , " + value
			return nil
		})
	}
	return strings.TrimPrefix(txtSort, " , ")
}
