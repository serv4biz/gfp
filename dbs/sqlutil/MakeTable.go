package sqlutil

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

func MakeTable(jsaTable *jsons.Array) string {
	txtTable := ""
	if jsaTable.Length() > 0 {
		jsaTable.EachString(func(index int, value string) error {
			txtTable += " , " + value
			return nil
		})
	}
	return strings.TrimPrefix(txtTable, " , ")
}
