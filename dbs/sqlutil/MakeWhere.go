package sqlutil

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

func MakeWhere(jsaWhere *jsons.Array) string {
	txtWhere := ""
	if jsaWhere.Length() > 0 {
		jsaWhere.EachString(func(index int, value string) error {
			txtWhere += " AND " + value
			return nil
		})
	}
	return strings.TrimPrefix(txtWhere, " AND ")
}
