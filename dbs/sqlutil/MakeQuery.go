package sqlutil

import (
	"gitlab.com/serv4biz/gfp/jsons"
)

func MakeQuery(jsaTable *jsons.Array, jsaColumn *jsons.Array, jsaWhere *jsons.Array, jsaOrder *jsons.Array, intOffset int, intLimit int) string {
	return SelectSQL(MakeTable(jsaTable), MakeColumn(jsaColumn), MakeWhere(jsaWhere), MakeOrder(jsaOrder), intOffset, intLimit)
}
