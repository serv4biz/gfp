package sqlutil

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// InsertSQL is automatic build insert sql
func InsertSQL(txtTable string, jsoData *jsons.Object) string {
	arrColumns := jsoData.GetKeys()
	txtColumns := ""
	txtValues := ""
	for _, columnName := range arrColumns {
		txtColumns = fmt.Sprint(txtColumns, columnName, ",")
		switch jsons.GetType(jsoData.Any(columnName)) {
		case jsons.TYPE_OBJECT:
			obj, _ := jsoData.GetObject(columnName)
			txtJSON, _ := obj.ToString()
			txtValues = fmt.Sprint(txtValues, Quote(txtJSON), ",")
		case jsons.TYPE_ARRAY:
			obj, _ := jsoData.GetArray(columnName)
			txtJSON, _ := obj.ToString()
			txtValues = fmt.Sprint(txtValues, Quote(txtJSON), ",")
		case jsons.TYPE_STRING:
			txtText, _ := jsoData.GetString(columnName)
			txtValues = fmt.Sprint(txtValues, Quote(txtText), ",")
		case jsons.TYPE_NUMBER:
			dblNumber, _ := jsoData.GetNumber(columnName)
			txtValues = fmt.Sprint(txtValues, dblNumber, ",")
		case jsons.TYPE_BOOL:
			isPass, _ := jsoData.GetBool(columnName)
			txtValues = fmt.Sprint(txtValues, isPass, ",")
		case jsons.TYPE_NULL:
			txtValues = fmt.Sprint(txtValues, "NULL,")
		}
	}
	txtColumns = strings.TrimSpace(strings.Trim(txtColumns, ","))
	txtValues = strings.TrimSpace(strings.Trim(txtValues, ","))
	return strings.TrimSpace(fmt.Sprint("INSERT INTO ", txtTable, " (", txtColumns, ") VALUES (", txtValues, ")"))
}
