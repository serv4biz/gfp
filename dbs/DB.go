package dbs

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"gitlab.com/serv4biz/gfp/collection"
	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/jsons"
)

func Open(driverName DriverName, dataSourceName string) (*DB, error) {
	conn, err := sql.Open(string(driverName), dataSourceName)
	if err != nil {
		return nil, err
	}

	err = conn.Ping()
	if err != nil {
		conn.Close()
		conn = nil
		return nil, err
	}

	dbx := new(DB)
	dbx.Conn = conn
	return dbx, nil
}

func Connect(driverName DriverName, host string, port int, username string, password string, dbName string) (*DB, error) {
	switch driverName {
	case DriverPostgreSQL:
		psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, dbName)
		return Open(driverName, psqlInfo)
	case DriverMySQL:
		psqlInfo := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", username, password, host, port, dbName)
		return Open(driverName, psqlInfo)
	}
	return nil, errors.New("database driver not support")
}

func (me *DB) Close() error {
	return me.Conn.Close()
}

func (me *DB) Ping() error {
	return me.Conn.Ping()
}

func (me *DB) Begin() (*TX, error) {
	tx, err := me.Conn.Begin()
	if err != nil {
		return nil, err
	}

	ntx := new(TX)
	ntx.ParentDB = me
	ntx.ConnTX = tx
	return ntx, nil
}

func (me *DB) RawScan(sql string, callback func(data map[string]any) error) error {
	dbRows, err := me.Conn.Query(sql)
	if err != nil {
		return err
	}
	defer dbRows.Close()

	arrColumns, err := dbRows.Columns()
	if err != nil {
		return err
	}

	datas := make([]any, len(arrColumns))
	dataPtrs := make([]any, len(arrColumns))

	for index := range arrColumns {
		dataPtrs[index] = &datas[index]
	}

	for dbRows.Next() {
		err := dbRows.Scan(dataPtrs...)
		if err != nil {
			return err
		}

		data := make(map[string]any)
		for i, k := range arrColumns {
			data[k] = datas[i]
		}

		err = callback(data)
		if err != nil {
			if err == errs.ErrBreak || strings.EqualFold(err.Error(), errs.ErrBreak.Error()) {
				break
			}
			return err
		}
	}
	return nil
}

func (me *DB) RawQuery(sql string) ([]map[string]any, error) {
	list := collection.ArrayListNew()
	err := me.RawScan(sql, func(data map[string]any) error {
		list.Add(data)
		return nil
	})
	result := make([]map[string]any, list.Length())

	list.Begin()
	for list.Next() {
		i, n := list.Fetch()
		result[i] = n.Get().(map[string]any)
	}
	return result, err
}

func (me *DB) RawFetch(sql string) (map[string]any, error) {
	var jsoItem map[string]any = nil
	err := me.RawScan(sql, func(item map[string]any) error {
		jsoItem = item
		return errs.ErrBreak
	})
	if err != nil {
		return nil, err
	}
	if jsoItem == nil {
		return nil, errors.New("no data")
	}
	return jsoItem, nil
}

func (me *DB) RawFetchRow(tableName string, columnName string, where string) (map[string]any, error) {
	txtSQL := sqlutil.SelectSQL(tableName, columnName, where, "", 0, 1)
	return me.RawFetch(txtSQL)
}

func (me *DB) RawSelectRow(tableName string, columnName string, where string, sort string, offset int, limit int) ([]map[string]any, error) {
	txtSQL := sqlutil.SelectSQL(tableName, columnName, where, sort, offset, limit)
	return me.RawQuery(txtSQL)
}

func (me *DB) Scan(sql string, callback func(obj *jsons.Object) error) error {
	return me.RawScan(sql, func(data map[string]any) error {
		obj, err := ParseJSON(data)
		if err != nil {
			return err
		}

		err = callback(obj)
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *DB) Query(sql string) (*jsons.Array, error) {
	list := collection.ArrayListNew()
	err := me.Scan(sql, func(item *jsons.Object) error {
		list.Add(item)
		return nil
	})
	if err != nil {
		return nil, err
	}
	result := jsons.Array(list.ToArray())
	return &result, nil
}

func (me *DB) Exec(sql string) (*ResultExec, error) {
	result, err := me.Conn.Exec(sql)
	if err != nil {
		return nil, err
	}

	LastInsertId, err := result.LastInsertId()
	if err != nil {
		LastInsertId = 0
	}

	RowsAffected, err := result.RowsAffected()
	if err != nil {
		RowsAffected = 0
	}

	res := new(ResultExec)
	res.LastInsertId = LastInsertId
	res.RowsAffected = RowsAffected
	return res, nil
}

func (me *DB) Fetch(sql string) (*jsons.Object, error) {
	var jsoItem *jsons.Object = nil
	err := me.Scan(sql, func(item *jsons.Object) error {
		jsoItem = item
		return errs.ErrBreak
	})
	if err != nil {
		return nil, err
	}
	if jsoItem == nil {
		return nil, errors.New("no data")
	}
	return jsoItem, nil
}

func (me *DB) Exist(sql string) (bool, error) {
	var jsoItem *jsons.Object = nil
	err := me.Scan(sql, func(item *jsons.Object) error {
		jsoItem = item
		return errs.ErrBreak
	})
	if err != nil {
		return false, err
	}
	return jsoItem != nil, nil
}

func (me *DB) FetchRow(tableName string, columnName string, where string) (*jsons.Object, error) {
	txtSQL := sqlutil.SelectSQL(tableName, columnName, where, "", 0, 1)
	return me.Fetch(txtSQL)
}

func (me *DB) ExistRow(tableName string, where string) (bool, error) {
	txtSQL := sqlutil.SelectSQL(tableName, "*", where, "", 0, 1)
	return me.Exist(txtSQL)
}

func (me *DB) SelectRow(tableName string, columnName string, where string, sort string, offset int, limit int) (*jsons.Array, error) {
	txtSQL := sqlutil.SelectSQL(tableName, columnName, where, sort, offset, limit)
	return me.Query(txtSQL)
}

func (me *DB) InsertRow(tableName string, jsoData *jsons.Object) (*ResultExec, error) {
	txtSQL := sqlutil.InsertSQL(tableName, jsoData)
	return me.Exec(txtSQL)
}

func (me *DB) UpdateRow(tableName string, jsoData *jsons.Object, where string) (*ResultExec, error) {
	txtSQL := sqlutil.UpdateSQL(tableName, jsoData, where)
	return me.Exec(txtSQL)
}

func (me *DB) DeleteRow(tableName string, where string) (*ResultExec, error) {
	txtSQL := sqlutil.DeleteSQL(tableName, where)
	return me.Exec(txtSQL)
}

func (me *DB) CountRow(tableName string, where string) (int, error) {
	jsoRow, err := me.FetchRow(tableName, "count(*) as int_count", where)
	if err != nil {
		return -1, err
	}
	count, ok := jsoRow.GetInt("int_count")
	if !ok {
		return -1, errors.New("no field int_count")
	}
	return count, nil
}
