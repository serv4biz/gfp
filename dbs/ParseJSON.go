package dbs

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"

	"gitlab.com/serv4biz/gfp/jsons"
)

func ParseJSON(data map[string]any) (*jsons.Object, error) {
	jsoData := jsons.ObjectNew(0)
	for key, val := range data {
		switch val := val.(type) {
		case string:
			jsoData.PutString(key, val)
		case float64:
			jsoData.PutNumber(key, val)
		case int64:
			jsoData.PutNumber(key, float64(val))
		case bool:
			jsoData.PutBool(key, val)
		case time.Time:
			jsoData.PutInt(key, int(val.Unix()))
		case []uint8:
			txtBuffer := strings.TrimSpace(string(val))
			if jsons.Valid(txtBuffer) {
				var obj any
				err := json.Unmarshal([]byte(txtBuffer), &obj)
				if err != nil {
					return nil, err
				}
				jsoData.PutAny(key, obj)
			} else {
				jsoData.PutAny(key, val)
			}
		case nil:
			jsoData.PutNull(key)
		default:
			refval := reflect.ValueOf(val)
			if refval.CanInt() || refval.CanFloat() {
				jsoData.SetNumber(key, jsons.AnyNumber(val))
			} else {
				return nil, fmt.Errorf("ParseJSON no support column %s = %s (%s)", key, val, reflect.TypeOf(val))
			}
		}
	}
	return jsoData, nil
}
