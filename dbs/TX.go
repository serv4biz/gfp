package dbs

import (
	"errors"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"gitlab.com/serv4biz/gfp/collection"
	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/jsons"
)

func (me *TX) Commit() error {
	return me.ConnTX.Commit()
}

func (me *TX) Rollback() error {
	return me.ConnTX.Rollback()
}

func (me *TX) RawScan(sql string, callback func(data map[string]any) error) error {
	dbRows, err := me.ConnTX.Query(sql)
	if err != nil {
		return err
	}
	defer dbRows.Close()

	arrColumns, err := dbRows.Columns()
	if err != nil {
		return err
	}

	datas := make([]any, len(arrColumns))
	dataPtrs := make([]any, len(arrColumns))

	for index := range arrColumns {
		dataPtrs[index] = &datas[index]
	}

	for dbRows.Next() {
		err := dbRows.Scan(dataPtrs...)
		if err != nil {
			return err
		}

		data := make(map[string]any)
		for i, k := range arrColumns {
			data[k] = datas[i]
		}

		err = callback(data)
		if err != nil {
			if err == errs.ErrBreak || strings.EqualFold(err.Error(), errs.ErrBreak.Error()) {
				break
			}
			return err
		}
	}
	return nil
}

func (me *TX) RawQuery(sql string) ([]map[string]any, error) {
	list := collection.ArrayListNew()
	err := me.RawScan(sql, func(data map[string]any) error {
		list.Add(data)
		return nil
	})
	result := make([]map[string]any, list.Length())

	list.Begin()
	for list.Next() {
		i, n := list.Fetch()
		result[i] = n.Get().(map[string]any)
	}
	return result, err
}

func (me *TX) RawFetch(sql string) (map[string]any, error) {
	var jsoItem map[string]any = nil
	err := me.RawScan(sql, func(item map[string]any) error {
		jsoItem = item
		return errs.ErrBreak
	})
	if err != nil {
		return nil, err
	}
	if jsoItem == nil {
		return nil, errors.New("no data")
	}
	return jsoItem, nil
}

func (me *TX) RawFetchRow(tableName string, columnName string, where string) (map[string]any, error) {
	txtSQL := sqlutil.SelectSQL(tableName, columnName, where, "", 0, 1)
	return me.RawFetch(txtSQL)
}

func (me *TX) RawSelectRow(tableName string, columnName string, where string, sort string, offset int, limit int) ([]map[string]any, error) {
	txtSQL := sqlutil.SelectSQL(tableName, columnName, where, sort, offset, limit)
	return me.RawQuery(txtSQL)
}

func (me *TX) Scan(sql string, callback func(obj *jsons.Object) error) error {
	return me.RawScan(sql, func(data map[string]any) error {
		obj, err := ParseJSON(data)
		if err != nil {
			return err
		}

		err = callback(obj)
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *TX) Query(txtSQL string) (*jsons.Array, error) {
	jsaList := collection.ArrayListNew()
	err := me.Scan(txtSQL, func(item *jsons.Object) error {
		jsaList.Add(item)
		return nil
	})
	if err != nil {
		return nil, err
	}
	result := jsons.Array(jsaList.ToArray())
	return &result, nil
}

func (me *TX) Exec(txtSQL string) (*ResultExec, error) {
	result, err := me.ConnTX.Exec(txtSQL)
	if err != nil {
		return nil, err
	}

	LastInsertId, err := result.LastInsertId()
	if err != nil {
		LastInsertId = -1
	}

	RowsAffected, err := result.RowsAffected()
	if err != nil {
		RowsAffected = -1
	}

	res := new(ResultExec)
	res.LastInsertId = LastInsertId
	res.RowsAffected = RowsAffected
	return res, nil
}

func (me *TX) Fetch(txtSQL string) (*jsons.Object, error) {
	var jsoItem *jsons.Object = nil
	err := me.Scan(txtSQL, func(item *jsons.Object) error {
		jsoItem = item
		return errs.ErrBreak
	})
	if err != nil {
		return nil, err
	}
	if jsoItem == nil {
		return nil, errors.New("no data")
	}
	return jsoItem, nil
}

func (me *TX) Exist(txtSQL string) (bool, error) {
	var jsoItem *jsons.Object = nil
	err := me.Scan(txtSQL, func(item *jsons.Object) error {
		jsoItem = item
		return errs.ErrBreak
	})
	if err != nil {
		return false, err
	}
	return jsoItem != nil, nil
}

func (me *TX) FetchRow(txtTable string, txtColumn string, txtWhere string) (*jsons.Object, error) {
	txtSQL := sqlutil.SelectSQL(txtTable, txtColumn, txtWhere, "", 0, 1)
	return me.Fetch(txtSQL)
}

func (me *TX) ExistRow(txtTable string, txtWhere string) (bool, error) {
	txtSQL := sqlutil.SelectSQL(txtTable, "*", txtWhere, "", 0, 1)
	return me.Exist(txtSQL)
}

func (me *TX) SelectRow(txtTable string, txtColumn string, txtWhere string, txtSort string, intOffset int, intLimit int) (*jsons.Array, error) {
	txtSQL := sqlutil.SelectSQL(txtTable, txtColumn, txtWhere, txtSort, intOffset, intLimit)
	return me.Query(txtSQL)
}

func (me *TX) InsertRow(txtTable string, jsoData *jsons.Object) (*ResultExec, error) {
	txtSQL := sqlutil.InsertSQL(txtTable, jsoData)
	return me.Exec(txtSQL)
}

func (me *TX) UpdateRow(txtTable string, jsoData *jsons.Object, txtWhere string) (*ResultExec, error) {
	txtSQL := sqlutil.UpdateSQL(txtTable, jsoData, txtWhere)
	return me.Exec(txtSQL)
}

func (me *TX) DeleteRow(txtTable string, txtWhere string) (*ResultExec, error) {
	txtSQL := sqlutil.DeleteSQL(txtTable, txtWhere)
	return me.Exec(txtSQL)
}

func (me *TX) CountRow(txtTable string, txtWhere string) (int, error) {
	jsoRow, err := me.FetchRow(txtTable, "count(*) as int_count", txtWhere)
	if err != nil {
		return -1, err
	}
	count, ok := jsoRow.GetInt("int_count")
	if !ok {
		return -1, errors.New("no field int_count")
	}
	return count, nil
}
