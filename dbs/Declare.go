package dbs

import (
	"database/sql"

	"gitlab.com/serv4biz/gfp/jsons"
)

type DriverName string

const (
	DriverPostgreSQL DriverName = "postgres"
	DriverMySQL      DriverName = "mysql"
)

type ResultExec struct {
	LastInsertId int64
	RowsAffected int64
}

type DB struct {
	Conn *sql.DB
}

type TX struct {
	ParentDB *DB
	ConnTX   *sql.Tx
}

type DX interface {
	RawScan(sql string, callback func(data map[string]any) error) error
	RawQuery(sql string) ([]map[string]any, error)
	RawFetch(sql string) (map[string]any, error)
	RawFetchRow(tableName string, columnName string, where string) (map[string]any, error)
	RawSelectRow(tableName string, columnName string, where string, sort string, offset int, limit int) ([]map[string]any, error)

	Scan(sql string, callback func(*jsons.Object) error) error
	Query(sql string) (*jsons.Array, error)
	Exec(sql string) (*ResultExec, error)
	Fetch(sql string) (*jsons.Object, error)
	Exist(sql string) (bool, error)
	FetchRow(tableName string, columnName string, where string) (*jsons.Object, error)
	ExistRow(tableName string, where string) (bool, error)
	SelectRow(tableName string, columnName string, where string, sort string, offset int, limit int) (*jsons.Array, error)
	InsertRow(tableName string, jsoData *jsons.Object) (*ResultExec, error)
	UpdateRow(tableName string, jsoData *jsons.Object, where string) (*ResultExec, error)
	DeleteRow(tableName string, where string) (*ResultExec, error)
	CountRow(tableName string, where string) (int, error)
}
