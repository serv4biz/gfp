package ulids

import (
	"math/rand"
	"time"

	"github.com/oklog/ulid/v2"
)

func New() (uid ulid.ULID, e error) {
	entropy := rand.New(rand.NewSource(time.Now().UTC().UnixNano()))
	ms := ulid.Timestamp(time.Now().UTC())
	return ulid.New(ms, entropy)
}
