package errs

import "errors"

var (
	ErrBreak        = errors.New("break")
	ErrNoPermission = errors.New("no permission")

	ErrValueInvalid     = errors.New("value invalid")
	ErrCantConvertValue = errors.New("can't convert value")
)
