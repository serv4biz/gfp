package hash

import (
	"crypto/md5"
	"fmt"
)

// MD5 is encode buffer byte to md5 string
func MD5(buffer []byte) string {
	return fmt.Sprintf("%x", md5.Sum(buffer))
}
