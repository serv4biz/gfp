package jsons

import (
	"strconv"
	"strings"
)

func AnyNumber(val any) float64 {
	switch GetType(val) {
	case TYPE_OBJECT:
		switch val := val.(type) {
		case *map[string]any:
			return float64(len(*val))
		case map[string]any:
			return float64(len(val))
		case *Object:
			return float64(len(*val))
		case Object:
			return float64(len(val))
		}
	case TYPE_ARRAY:
		switch val := val.(type) {
		case *[]any:
			return float64(len(*val))
		case []any:
			return float64(len(val))
		case *Array:
			return float64(len(*val))
		case Array:
			return float64(len(val))
		}
	case TYPE_STRING:
		nval, err := strconv.ParseFloat(strings.TrimSpace(val.(string)), 64)
		if err != nil {
			return 0.0
		}
		return nval
	case TYPE_NUMBER:
		switch val := val.(type) {
		case float64:
			return float64(val)
		case float32:
			return float64(val)
		case int:
			return float64(val)
		case int8:
			return float64(val)
		case int16:
			return float64(val)
		case int32:
			return float64(val)
		case int64:
			return float64(val)
		case uint:
			return float64(val)
		case uint8:
			return float64(val)
		case uint16:
			return float64(val)
		case uint32:
			return float64(val)
		case uint64:
			return float64(val)
		case uintptr:
			return float64(val)
		}
	case TYPE_BOOL:
		if val.(bool) {
			return 1
		} else {
			return 0
		}
	case TYPE_NULL:
		return 0.0
	}

	return 0.0
}
