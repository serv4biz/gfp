package jsons

import (
	"encoding/json"
	"io/fs"
	"strings"

	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/files"
)

func ArrayNew(size int) *Array {
	list := make(Array, size)
	return &list
}

func ArrayFromByte(buffer []byte) (*Array, error) {
	var data *Array = ArrayNew(0)
	err := json.Unmarshal(buffer, data)
	if err != nil {
		return nil, err
	}
	return data, err
}

func ArrayFromString(code string) (*Array, error) {
	return ArrayFromByte([]byte(code))
}

func ArrayFromFile(pathFile string) (*Array, error) {
	buffer, err := files.ReadFile(pathFile)
	if err != nil {
		return nil, err
	}
	return ArrayFromByte(buffer)
}

func (me *Array) FromByte(buffer []byte) error {
	return json.Unmarshal(buffer, me)
}

func (me *Array) FromString(code string) error {
	return me.FromByte([]byte(code))
}

func (me *Array) FromFile(pathFile string) error {
	buffer, err := files.ReadFile(pathFile)
	if err != nil {
		return err
	}
	return me.FromByte(buffer)
}

func (me *Array) Clone() (*Array, error) {
	txtBuffer, err := me.ToString()
	if err != nil {
		return nil, err
	}
	return ArrayFromString(txtBuffer)
}

func (me *Array) GetType(index int) Type {
	return GetType((*me)[index])
}

func (me *Array) Length() int {
	return len(*me)
}

func (me *Array) Delete(index int) *Array {
	n := ArrayNew(len(*me) - 1)
	count := 0
	for i, val := range *me {
		if index != i {
			(*n)[count] = val
			count++
		}
	}
	*me = *n
	return me
}

func (me *Array) Remove(index int) *Array {
	return me.Delete(index)
}

func (me *Array) ToBytes() ([]byte, error) {
	buffer, err := json.Marshal(*me)
	return buffer, err
}

func (me *Array) ToString() (string, error) {
	buffer, err := me.ToBytes()
	return string(buffer), err
}

func (me *Array) ToFile(pathFile string) (int, error) {
	buffer, err := me.ToBytes()
	if err != nil {
		return -1, err
	}
	return files.WriteFile(pathFile, buffer, fs.ModePerm)
}

func (me *Array) SetObject(index int, value *Object) *Array {
	(*me)[index] = value
	return me
}

func (me *Array) SetArray(index int, value *Array) *Array {
	(*me)[index] = value
	return me
}

func (me *Array) SetString(index int, value string) *Array {
	(*me)[index] = value
	return me
}

func (me *Array) SetNumber(index int, value float64) *Array {
	(*me)[index] = value
	return me
}

func (me *Array) SetInt(index int, value int) *Array {
	(*me)[index] = float64(value)
	return me
}

func (me *Array) SetBool(index int, value bool) *Array {
	(*me)[index] = value
	return me
}

func (me *Array) SetNull(index int) *Array {
	(*me)[index] = nil
	return me
}

func (me *Array) AddObject(value *Object) *Array {
	*me = append(*me, value)
	return me
}

func (me *Array) AddArray(value *Array) *Array {
	*me = append(*me, value)
	return me
}

func (me *Array) AddString(value string) *Array {
	*me = append(*me, value)
	return me
}

func (me *Array) AddNumber(value float64) *Array {
	*me = append(*me, value)
	return me
}

func (me *Array) AddInt(value int) *Array {
	*me = append(*me, float64(value))
	return me
}

func (me *Array) AddBool(value bool) *Array {
	*me = append(*me, value)
	return me
}

func (me *Array) AddNull() *Array {
	*me = append(*me, nil)
	return me
}

func (me *Array) AddAny(value any) *Array {
	*me = append(*me, value)
	return me
}

func (me *Array) PutObject(value *Object) *Array {
	return me.AddObject(value)
}

func (me *Array) PutArray(value *Array) *Array {
	return me.AddArray(value)
}

func (me *Array) PutString(value string) *Array {
	return me.AddString(value)
}

func (me *Array) PutNumber(value float64) *Array {
	return me.AddNumber(value)
}

func (me *Array) PutInt(value int) *Array {
	return me.AddInt(value)
}

func (me *Array) PutBool(value bool) *Array {
	return me.AddBool(value)
}

func (me *Array) PutNull() *Array {
	return me.AddNull()
}

func (me *Array) PutAny(value any) *Array {
	return me.AddAny(value)
}

func (me *Array) GetObject(index int) *Object {
	val := (*me)[index]
	switch val := val.(type) {
	case *map[string]any:
		obj := Object(*val)
		return &obj
	case map[string]any:
		obj := Object(val)
		return &obj
	case *Object:
		return val
	case Object:
		return &val
	}
	return nil
}

func (me *Array) GetArray(index int) *Array {
	val := (*me)[index]
	switch val := val.(type) {
	case *[]any:
		list := Array(*val)
		return &list
	case []any:
		list := Array(val)
		return &list
	case *Array:
		return val
	case Array:
		return &val
	}
	return nil
}

func (me *Array) GetString(index int) string {
	val := (*me)[index]
	return val.(string)
}

func (me *Array) GetNumber(index int) float64 {
	val := (*me)[index]
	return AnyNumber(val)
}

func (me *Array) GetInt(index int) int {
	val := (*me)[index]
	return int(AnyNumber(val))
}

func (me *Array) GetBool(index int) bool {
	val := (*me)[index]
	return val.(bool)
}

func (me *Array) GetNull(index int) any {
	val := (*me)[index]
	return val
}

func (me *Array) GetAny(index int) any {
	val := (*me)[index]
	return val
}

func (me *Array) Object(index int) *Object {
	val := (*me)[index]
	return AnyObject(val)
}

func (me *Array) Array(index int) *Array {
	val := (*me)[index]
	return AnyArray(val)
}

func (me *Array) String(index int) string {
	val := (*me)[index]
	return AnyString(val)
}

func (me *Array) Number(index int) float64 {
	val := (*me)[index]
	return AnyNumber(val)
}

func (me *Array) Int(index int) int {
	return int((*me).Number(index))
}

func (me *Array) Bool(index int) bool {
	val := (*me)[index]
	return AnyBool(val)
}

func (me *Array) Any(index int) any {
	return (*me)[index]
}

func (me *Array) Each(callback func(index int, value any) error) error {
	for i, v := range *me {
		err := callback(i, v)
		if err != nil {
			if err == errs.ErrBreak || strings.EqualFold(err.Error(), errs.ErrBreak.Error()) {
				break
			}
			return err
		}
	}
	return nil
}

func (me *Array) EachAny(callback func(index int, value any) error) error {
	return me.Each(func(index int, value any) error {
		err := callback(index, value)
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Array) EachObject(callback func(index int, value *Object) error) error {
	return me.EachAny(func(index int, value any) error {
		err := callback(index, AnyObject(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Array) EachArray(callback func(index int, value *Array) error) error {
	return me.EachAny(func(index int, value any) error {
		err := callback(index, AnyArray(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Array) EachString(callback func(index int, value string) error) error {
	return me.EachAny(func(index int, value any) error {
		err := callback(index, AnyString(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Array) EachNumber(callback func(index int, value float64) error) error {
	return me.EachAny(func(index int, value any) error {
		err := callback(index, AnyNumber(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Array) EachInt(callback func(index int, value int) error) error {
	return me.EachAny(func(index int, value any) error {
		err := callback(index, AnyInt(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Array) EachBool(callback func(index int, value bool) error) error {
	return me.EachAny(func(index int, value any) error {
		err := callback(index, AnyBool(value))
		if err != nil {
			return err
		}
		return nil
	})
}
