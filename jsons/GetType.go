package jsons

func GetType(data any) Type {
	if data == nil {
		return TYPE_NULL
	}

	switch data.(type) {
	case *map[string]any:
		return TYPE_OBJECT
	case map[string]any:
		return TYPE_OBJECT
	case *Object:
		return TYPE_OBJECT
	case Object:
		return TYPE_OBJECT
	case *[]any:
		return TYPE_ARRAY
	case []any:
		return TYPE_ARRAY
	case *Array:
		return TYPE_ARRAY
	case Array:
		return TYPE_ARRAY
	case string:
		return TYPE_STRING
	case bool:
		return TYPE_BOOL
	case float64:
		return TYPE_NUMBER
	case float32:
		return TYPE_NUMBER
	case int:
		return TYPE_NUMBER
	case int8:
		return TYPE_NUMBER
	case int16:
		return TYPE_NUMBER
	case int32:
		return TYPE_NUMBER
	case int64:
		return TYPE_NUMBER
	case uint:
		return TYPE_NUMBER
	case uint8:
		return TYPE_NUMBER
	case uint16:
		return TYPE_NUMBER
	case uint32:
		return TYPE_NUMBER
	case uint64:
		return TYPE_NUMBER
	case uintptr:
		return TYPE_NUMBER
	}
	return TYPE_NULL
}
