package jsons

func AnyArray(val any) *Array {
	switch GetType(val) {
	case TYPE_OBJECT:
		arr := ArrayNew(0)
		arr.PutAny(val)
		return arr
	case TYPE_ARRAY:
		switch val := val.(type) {
		case *[]any:
			list := Array(*val)
			return &list
		case []any:
			list := Array(val)
			return &list
		case *Array:
			return val
		case Array:
			return &val
		}
	case TYPE_STRING:
		arr := ArrayNew(0)
		arr.PutAny(val)
		return arr
	case TYPE_NUMBER:
		arr := ArrayNew(0)
		arr.PutAny(val)
		return arr
	case TYPE_BOOL:
		arr := ArrayNew(0)
		arr.PutAny(val)
		return arr
	case TYPE_NULL:
		arr := ArrayNew(0)
		arr.PutAny(val)
		return arr
	}
	return ArrayNew(0)
}
