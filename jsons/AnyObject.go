package jsons

func AnyObject(val any) *Object {
	switch GetType(val) {
	case TYPE_OBJECT:
		switch val := val.(type) {
		case *map[string]any:
			obj := Object(*val)
			return &obj
		case map[string]any:
			obj := Object(val)
			return &obj
		case *Object:
			return val
		case Object:
			return &val
		}
	case TYPE_ARRAY:
		obj := ObjectNew(0)
		obj.SetAny("value", val)
		return obj
	case TYPE_STRING:
		obj := ObjectNew(0)
		obj.SetAny("value", val)
		return obj
	case TYPE_NUMBER:
		obj := ObjectNew(0)
		obj.SetAny("value", val)
		return obj
	case TYPE_BOOL:
		obj := ObjectNew(0)
		obj.SetAny("value", val)
		return obj
	case TYPE_NULL:
		obj := ObjectNew(0)
		obj.SetAny("value", val)
		return obj
	}
	return ObjectNew(0)
}
