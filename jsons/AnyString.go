package jsons

import (
	"encoding/json"
	"fmt"
)

func AnyString(val any) string {
	switch GetType(val) {
	case TYPE_OBJECT:
		switch val := val.(type) {
		case *map[string]any:
			bval, err := json.Marshal(*val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		case map[string]any:
			bval, err := json.Marshal(val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		case *Object:
			bval, err := json.Marshal(*val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		case Object:
			bval, err := json.Marshal(val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		}
	case TYPE_ARRAY:
		switch val := val.(type) {
		case *[]any:
			bval, err := json.Marshal(*val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		case []any:
			bval, err := json.Marshal(val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		case *Array:
			bval, err := json.Marshal(*val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		case Array:
			bval, err := json.Marshal(val)
			if err != nil {
				return fmt.Sprint(val)
			}
			return string(bval)
		}
	case TYPE_STRING:
		return val.(string)
	case TYPE_NUMBER:
		return fmt.Sprint(AnyNumber(val))
	case TYPE_BOOL:
		return fmt.Sprint(val)
	case TYPE_NULL:
		return "null"
	}
	return ""
}
