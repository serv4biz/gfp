package jsons

type Type int
type Object map[string]any
type Array []any

const (
	TYPE_NULL Type = iota
	TYPE_OBJECT
	TYPE_ARRAY
	TYPE_STRING
	TYPE_NUMBER
	TYPE_BOOL
)
