package jsons

import "strings"

func AnyBool(val any) bool {
	switch GetType(val) {
	case TYPE_OBJECT:
		switch val := val.(type) {
		case *map[string]any:
			return len(*val) > 0
		case map[string]any:
			return len(val) > 0
		case *Object:
			return len(*val) > 0
		case Object:
			return len(val) > 0
		}
	case TYPE_ARRAY:
		switch val := val.(type) {
		case *[]any:
			return len(*val) > 0
		case []any:
			return len(val) > 0
		case *Array:
			return len(*val) > 0
		case Array:
			return len(val) > 0
		}
	case TYPE_STRING:
		data := strings.ToLower(strings.TrimSpace(val.(string)))
		return data == "true"
	case TYPE_NUMBER:
		return AnyNumber(val) > 0
	case TYPE_BOOL:
		return val.(bool)
	case TYPE_NULL:
		return false
	}

	return false
}
