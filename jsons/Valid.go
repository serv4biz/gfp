package jsons

import "encoding/json"

func Valid(code string) bool {
	return json.Valid([]byte(code))
}
