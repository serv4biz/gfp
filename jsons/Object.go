package jsons

import (
	"encoding/json"
	"io/fs"
	"strings"

	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/files"
)

func ObjectNew(size int) *Object {
	obj := make(Object, size)
	return &obj
}

func ObjectFromByte(buffer []byte) (*Object, error) {
	var data *Object = ObjectNew(0)
	err := json.Unmarshal(buffer, data)
	if err != nil {
		return nil, err
	}
	return data, err
}

func ObjectFromString(code string) (*Object, error) {
	return ObjectFromByte([]byte(code))
}

func ObjectFromFile(pathFile string) (*Object, error) {
	buffer, err := files.ReadFile(pathFile)
	if err != nil {
		return nil, err
	}
	return ObjectFromByte(buffer)
}

func (me *Object) FromByte(buffer []byte) error {
	err := json.Unmarshal(buffer, me)
	return err
}

func (me *Object) FromString(code string) error {
	return me.FromByte([]byte(code))
}

func (me *Object) FromFile(pathFile string) error {
	buffer, err := files.ReadFile(pathFile)
	if err != nil {
		return err
	}
	return me.FromByte(buffer)
}

func (me *Object) Clone() (*Object, error) {
	txtBuffer, err := me.ToString()
	if err != nil {
		return nil, err
	}
	return ObjectFromString(txtBuffer)
}

func (me *Object) Check(key string) bool {
	_, ok := (*me)[key]
	return ok
}

func (me *Object) GetType(key string) (Type, bool) {
	val, ok := (*me)[key]
	return GetType(val), ok
}

func (me *Object) GetKeys() []string {
	list := make([]string, len(*me))
	i := 0
	for key := range *me {
		list[i] = key
		i++
	}
	return list
}

func (me *Object) Length() int {
	return len(*me)
}

func (me *Object) Delete(key string) *Object {
	delete(*me, key)
	return me
}

func (me *Object) Remove(key string) *Object {
	me.Delete(key)
	return me
}

func (me *Object) ToBytes() ([]byte, error) {
	buffer, err := json.Marshal(*me)
	return buffer, err
}

func (me *Object) ToString() (string, error) {
	buffer, err := me.ToBytes()
	return string(buffer), err
}

func (me *Object) ToFile(pathFile string) (int, error) {
	buffer, err := me.ToBytes()
	if err != nil {
		return -1, err
	}
	return files.WriteFile(pathFile, buffer, fs.ModePerm)
}

func (me *Object) SetObject(key string, value *Object) *Object {
	(*me)[key] = value
	return me
}

func (me *Object) SetArray(key string, value *Array) *Object {
	(*me)[key] = value
	return me
}

func (me *Object) SetString(key string, value string) *Object {
	(*me)[key] = value
	return me
}

func (me *Object) SetNumber(key string, value float64) *Object {
	(*me)[key] = float64(value)
	return me
}

func (me *Object) SetInt(key string, value int) *Object {
	(*me)[key] = float64(value)
	return me
}

func (me *Object) SetBool(key string, value bool) *Object {
	(*me)[key] = value
	return me
}

func (me *Object) SetNull(key string) *Object {
	(*me)[key] = nil
	return me
}

func (me *Object) SetAny(key string, value any) *Object {
	(*me)[key] = value
	return me
}

func (me *Object) PutObject(key string, value *Object) *Object {
	return me.SetObject(key, value)
}

func (me *Object) PutArray(key string, value *Array) *Object {
	return me.SetArray(key, value)
}

func (me *Object) PutString(key string, value string) *Object {
	return me.SetString(key, value)
}

func (me *Object) PutNumber(key string, value float64) *Object {
	return me.SetNumber(key, value)
}

func (me *Object) PutInt(key string, value int) *Object {
	return me.SetInt(key, value)
}

func (me *Object) PutBool(key string, value bool) *Object {
	return me.SetBool(key, value)
}

func (me *Object) PutNull(key string) *Object {
	return me.SetNull(key)
}

func (me *Object) PutAny(key string, value any) *Object {
	return me.SetAny(key, value)
}

func (me *Object) GetObject(key string) (*Object, bool) {
	val, ok := (*me)[key]
	if !ok {
		return nil, false
	}

	switch val := val.(type) {
	case *map[string]any:
		obj := Object(*val)
		return &obj, ok
	case map[string]any:
		obj := Object(val)
		return &obj, ok
	case *Object:
		return val, ok
	case Object:
		return &val, ok
	}
	return nil, false
}

func (me *Object) GetArray(key string) (*Array, bool) {
	val, ok := (*me)[key]
	if !ok {
		return nil, false
	}

	switch val := val.(type) {
	case *[]any:
		list := Array(*val)
		return &list, ok
	case []any:
		list := Array(val)
		return &list, ok
	case *Array:
		return val, ok
	case Array:
		return &val, ok
	}
	return nil, false
}

func (me *Object) GetString(key string) (string, bool) {
	val, ok := (*me)[key]
	if !ok {
		return "", false
	}
	return val.(string), ok
}

func (me *Object) GetNumber(key string) (float64, bool) {
	val, ok := (*me)[key]
	if !ok {
		return 0, false
	}
	return AnyNumber(val), ok
}

func (me *Object) GetInt(key string) (int, bool) {
	val, ok := (*me)[key]
	if !ok {
		return 0, false
	}

	return int(AnyNumber(val)), ok
}

func (me *Object) GetBool(key string) (bool, bool) {
	val, ok := (*me)[key]
	if !ok {
		return false, false
	}

	return val.(bool), ok
}

func (me *Object) GetNull(key string) (any, bool) {
	val, ok := (*me)[key]
	if !ok {
		return nil, false
	}
	return val, ok
}

func (me *Object) GetAny(key string) (any, bool) {
	val, ok := (*me)[key]
	if !ok {
		return nil, false
	}
	return val, ok
}

func (me *Object) Object(key string) *Object {
	val, ok := (*me)[key]
	if !ok {
		return ObjectNew(0)
	}
	return AnyObject(val)
}

func (me *Object) Array(key string) *Array {
	val, ok := (*me)[key]
	if !ok {
		return ArrayNew(0)
	}
	return AnyArray(val)
}

func (me *Object) String(key string) string {
	val, ok := (*me)[key]
	if !ok {
		return ""
	}
	return AnyString(val)
}

func (me *Object) Number(key string) float64 {
	val, ok := (*me)[key]
	if !ok {
		return 0.0
	}
	return AnyNumber(val)
}

func (me *Object) Int(key string) int {
	return int((*me).Number(key))
}

func (me *Object) Bool(key string) bool {
	val, ok := (*me)[key]
	if !ok {
		return false
	}
	return AnyBool(val)
}

func (me *Object) Any(key string) any {
	val, ok := (*me)[key]
	if !ok {
		return nil
	}
	return val
}

func (me *Object) Each(callback func(key string, value any) error) error {
	for k, v := range *me {
		err := callback(k, v)
		if err != nil {
			if err == errs.ErrBreak || strings.EqualFold(err.Error(), errs.ErrBreak.Error()) {
				break
			}
			return err
		}
	}
	return nil
}

func (me *Object) EachAny(callback func(key string, value any) error) error {
	return me.Each(func(key string, value any) error {
		err := callback(key, value)
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Object) EachObject(callback func(key string, value *Object) error) error {
	return me.EachAny(func(key string, value any) error {
		err := callback(key, AnyObject(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Object) EachArray(callback func(key string, value *Array) error) error {
	return me.EachAny(func(key string, value any) error {
		err := callback(key, AnyArray(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Object) EachString(callback func(key string, value string) error) error {
	return me.EachAny(func(key string, value any) error {
		err := callback(key, AnyString(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Object) EachNumber(callback func(key string, value float64) error) error {
	return me.EachAny(func(key string, value any) error {
		err := callback(key, AnyNumber(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Object) EachInt(callback func(key string, value int) error) error {
	return me.EachAny(func(key string, value any) error {
		err := callback(key, AnyInt(value))
		if err != nil {
			return err
		}
		return nil
	})
}

func (me *Object) EachBool(callback func(key string, value bool) error) error {
	return me.EachAny(func(key string, value any) error {
		err := callback(key, AnyBool(value))
		if err != nil {
			return err
		}
		return nil
	})
}
