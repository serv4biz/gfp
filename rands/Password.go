package rands

import (
	"fmt"
	rand "math/rand/v2"
)

const (
	PASSWORD_NUMBER int = iota
	PASSWORD_LOWERCASE
	PASSWORD_UPPERCASE
	PASSWORD_SPECIAL
)

// Password is random string of level
func Password(n int, fmts ...int) string {
	letterBytes := ""
	letterNumber := "0123456789"
	letterLowerCase := "abcdefghijklmnopqrstuvwxyz"
	letterUpperCase := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	letterSpecial := "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"

	if len(fmts) > 0 {
		for _, fmt := range fmts {
			switch fmt {
			case PASSWORD_NUMBER:
				letterBytes += letterNumber
			case PASSWORD_LOWERCASE:
				letterBytes += letterLowerCase
			case PASSWORD_UPPERCASE:
				letterBytes += letterUpperCase
			case PASSWORD_SPECIAL:
				letterBytes += letterSpecial
			}
		}
	} else {
		letterBytes = letterNumber
	}

	result := ""
	for i := 0; i < n; i++ {
		result = fmt.Sprint(result, string(letterBytes[rand.IntN(len(letterBytes))]))
	}
	return result
}
