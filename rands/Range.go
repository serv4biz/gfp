package rands

import (
	rand "math/rand/v2"
)

func Range(min int, max int) int {
	return rand.IntN(max-min+1) + min
}
