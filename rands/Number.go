package rands

import (
	rand "math/rand/v2"
)

func Number(num int) int {
	return rand.IntN(num)
}
