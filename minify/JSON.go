package minify

import (
	"errors"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

func JSON(source string) (string, error) {
	result := strings.TrimSpace(source)
	runes := []rune(result)

	if runes[0] == '{' {
		jsoResult, err := jsons.ObjectFromString(result)
		if err != nil {
			return "", err
		}
		return jsoResult.ToString()
	} else if runes[0] == '[' {
		jsaResult, err := jsons.ArrayFromString(result)
		if err != nil {
			return "", err
		}
		return jsaResult.ToString()
	}
	return "", errors.New("source format is not support")
}
