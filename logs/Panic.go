package logs

import (
	"bytes"
	"fmt"
	"io/fs"
	"os"
	"runtime"
	"time"

	"gitlab.com/serv4biz/gfp/files"
)

func Panic(err error, writeToFile bool) {
	if err != nil {
		dt := time.Now().UTC()

		pc, fn, line, _ := runtime.Caller(1)
		strLine := fmt.Sprintf("%s [%s] in %s[%s:%d] %v \r\n", dt.Format(time.DateTime), LogTypePanic, runtime.FuncForPC(pc).Name(), fn, line, err)
		fmt.Print(strLine)

		if writeToFile {
			MutexLog.Lock()
			defer MutexLog.Unlock()

			strFilename := fmt.Sprintf("%04d-%02d-%02d.log", dt.Year(), dt.Month(), dt.Day())
			strPathApp, err := files.GetAppDir()
			if err != nil {
				panic(err)
			}

			strPathDir := fmt.Sprint(strPathApp, "/logs")
			strPathFile := fmt.Sprint(strPathDir, "/", strFilename)
			files.MakeDir(strPathDir, fs.ModePerm)

			var bf bytes.Buffer
			if files.ExistFile(strPathFile) {
				buffer, err := files.ReadFile(strPathFile)
				if err != nil {
					fmt.Println(err)
				} else {
					bf.Write(buffer)
				}
			}
			_, err = bf.WriteString(strLine)
			if err != nil {
				fmt.Println(err)
			}

			_, err = files.WriteFile(strPathFile, bf.Bytes(), fs.ModePerm)
			if err != nil {
				fmt.Println(err)
			}
		}
		os.Exit(2)
	}
}
