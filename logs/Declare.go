package logs

import "sync"

type LogType string

const (
	LogTypeAlert   LogType = "alert"
	LogTypeInfo    LogType = "info"
	LogTypeWarning LogType = "warning"
	LogTypeError   LogType = "error"
	LogTypePanic   LogType = "panic"
)

var MutexLog sync.RWMutex
