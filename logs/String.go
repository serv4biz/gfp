package logs

import (
	"fmt"
	"runtime"
	"time"
)

func String(typeName LogType, err error) string {
	if err != nil {
		dt := time.Now().UTC()

		pc, fn, line, _ := runtime.Caller(1)
		return fmt.Sprintf("%s [%s] in %s[%s:%d] %v \r\n", dt.Format(time.DateTime), typeName, runtime.FuncForPC(pc).Name(), fn, line, err)
	}
	return ""
}
