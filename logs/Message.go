package logs

import (
	"fmt"
	"runtime"
)

func Message(err error) string {
	if err == nil {
		return ""
	}

	_, _, line, _ := runtime.Caller(1)
	return fmt.Sprint(line, " : ", err)
}
